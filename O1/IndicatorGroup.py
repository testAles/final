# Classe énumérant les types d'indicateurs


from enum import Enum


class IndicatorGroup(Enum) :
    
    EXPORTS_AND_IMPORTS = 1
    SUPPLY_AND_USE = 2
    PRICES = 3
    FEED_PRICE_RATIO = 4
    QUANTITIES_FED = 5
    TRANSPORTATION = 6
    ANIMAL_UNIT_INDEXES = 7
 