# Classe représentant les spécificités de chaque mesure (année, valeur, périodicité, culture vivière concernée, indicateur de mesure, unité de la valeur retournée par l'indicateur)

import Commodity
import Indicator
import MeasurementType

class Measurement:

    
    def __init__(self, year : int, value : float, timeperiodld : int, timeperiodDescr : str, type : MeasurementType, commodity : Commodity, indicator : Indicator):
        self.year = year
        self.value =value
        self.timeperiodld = timeperiodld
        self.timeperiodDescr = timeperiodDescr
      