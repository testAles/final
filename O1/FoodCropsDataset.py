# point d'entrée principal
#import commodityGroup
import FoodCropFactory
import pandas

class FoodCropsDataset :
    
    def __init__( self, factory : FoodCropFactory ):
        self.factory = factory
        self.commodityGroupMeasurementIndex = dict()
        self.indicatorGroupMeasurementIndex = dict()
        self.locationMeasurementIndex = dict()
        self.unitMeasurementIndex = dict()
        
    def load(self, datasetPath : str):
        dataframe = pandas.read_csv("C:/Users/arthu/Documents/Foodcrop/FeedGrains.csv")
        for index, row in dataframe.iterrows():
            unit = self.factory.string_to_unit(row['SC_Unit_Desc'])
            commodityGroup = self.CommodityGroup[row["SC_GroupCommod_Desc"]]
            indicatorGroup = self.IndicatorGroup[row["SC_Group_Desc"]]
            commodity = self.factory.createCommodity(commodityGroup,int(row["SC_Commodity_ID"]),row["SC_Commodity_Desc"])
            indicator = self.factory.createIndicator(row["SC_Group_ID"], int(row["SC_Frequency_ID"]), row["SC_Frequency_Desc"], row["SC_GeographyIndented_Desc"], indicatorGroup, unit)
            measurementType = self.factory.createMeasurementType(int(row["SC_Attribute_ID"]),row["SC_Attribute_Desc"])
            
            if self.commodityGroupMeasurementIndex.has_key(commodityGroup):
                self.commodityGroupMeasurementIndex.value(commodityGroup).append(index)
            else :
                self.commodityGroupMeasurementIndex[commodityGroup] = [index]
            
            if self.indicatorGroupMeasurementIndex.has_key(indicatorGroup):
                self.indicatorGroupMeasurementIndex.value(indicatorGroup).append(index)
            else :
                self.indicatorGroupMeasurementIndex[indicatorGroup] = [index]
            
            location = row["SC_GeographyIndented_Desc"]
            if self.locationMeasurementIndex.has_key(location):
                self.locationMeasurementIndex.value(location).append(index)
            else :
                self.locationMeasurementIndex[location] = [index]
            
            if self.unitMeasurementIndex.has_key(unit):
                self.unitMeasurementIndex.value(unit).append(index)
            else :
                self.unitMeasurementIndex[unit] = [index]
            
            self.factory.createMeasurement(index, int(row["Year_ID"]), row["Amount"], int(row["Timeperiod_ID"]), row["Timeperiod_Desc"], measurementType, commodity, indicator)
    #def findMeasurements(commodityType : CommodityType = None, indicatorGroup : IndicatorGroup = None, geographicalLocation : str = None, unit : Unit = None) -> list :
        