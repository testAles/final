#Cette classe va créer les instances de classes nécessaire au moment de l'appel de la methode load par FoodCropDataSet

import Unit
import Commodity
import CommodityGroup
import Indicator
import IndicatorGroup
import MeasurementType
import Measurement

class FoodCropFactory :
    
    def __init__(self):
        self.unitsRegistry = dict()
        self.IndicatorsRegistry = dict()
        self.commodityRegistry = dict()
        self.measurementTypeRegistry = dict()
        
    def string_to_unit(self, string : str) -> Unit :
        switch = {
                "Million bushels": self.createVolume(1),
                "Million acres": self.createSurface(2),
                "Bushels": self.createVolume(3),
                "Dollars per bushel": self.createUnitRatio(4,self.Price,self.Volume),
                "Dollars per cwt": self.createUnitRatio(5,self.Price,self.Weight),
                "Bushels per acre": self.createUnitRatio(6,self.Volume,self.Surface),
                "1,000 metric tons": self.createWeight(7),
                "Million metric tons": self.createWeight(8),
                "1,000 tons": self.createWeight(9),
                "1,000 acres": self.createSurface(10),
                "Tons per acre": self.createUnitRatio(11,self.Weight,self.Surface),
                "Dollars per ton": self.createUnitRatio(12,self.Price,self.Weight),
                "Ratio": self.createRatio(13),
                "Cents per pound": self.createRatio(14),
                "Index (1984=100)": self.createCount(15,"index"),
                "Carloads originated": self.createCount(16,"carloads originated"),
                "1,000 liters": self.createVolume(17),
                "Gallons": self.createVolume(18),
                "Dollars per short ton": self.createUnitRatio(31,self.Price,self.Weight),
                "Ton": self.createWeight(41),
                "1,000 hectare": self.createSurface(44),
                "Metric tons per hectare": self.createUnitRatio(45,self.Weight,self.Surface),
                "Million animal units": self.createCount(46, "animal unit")
                }
        return switch.get(string, "Invalid unit")
    
    def createVolume(self, id : int) -> Unit :
        
        if not id in self.unitsRegistry.key() :
            volume = self.Volume(id)
            self.unitsRegistry.add[id] = volume

        return self.unitsRegistry(id)
    
    def createPrice(self, id : int) -> Unit :
        
        if not id in self.unitsRegistry.key() :
            price = self.Price(id)
            self.unitsRegistry.add[id] = price

        return self.unitsRegistry(id)    
    
    
    def createWeight(self, id : int, wheight : float) -> Unit :
        
        if not id in self.unitsRegistry.key() :
            weight1 = self.Weight(id, "Weight", wheight)
            self.unitsRegistry.add[id] = weight1

        return self.unitsRegistry(id)    
    
    
    def createSurface(self, id : int) -> Unit :
        
        if not id in self.unitsRegistry.key() :
            surface = self.Surface(id)
            self.unitsRegistry.add[id] = surface

        return self.unitsRegistry(id)    
    
    def createCount(self, id : int, what : str) -> Unit :
        if not id in self.unitsRegistry.key() :
            count = self.Count(id, "Count", what)
            self.unitsRegistry.add[id] = count

        return self.unitsRegistry(id) 
    
    
    def createRatio(self, id : int) -> Unit :
        
        if not id in self.unitsRegistry.key() :
            ratio = self.Ratio(id)
            self.unitsRegistry.add[id] = ratio

        return self.unitsRegistry(id) 
    
    
    def createUnitRatio(self, id : int, unit1 : Unit, unit2 : Unit) -> Unit :
        if not id in self.unitsRegistry.key() :
            unitratio = self.UnitRatio(id, unit1, unit2)
            self.unitsRegistry.add[id] = unitratio

        return self.unitsRegistry(id)    
    
    
    def createCommodity(self, group : CommodityGroup, id : int, name : str ) -> Commodity :
        
        if not id in self.commodityRegistry.key() :
            commodity = Commodity(CommodityGroup, id, name)
            self.commodityRegistry.add[id] = commodity

        return self.commodityRegistry(id)
        
    def createIndicator(self, id : int, frequency : int, freqDesc: str, geogLocation: str, indicatorGroup: IndicatorGroup, unit: Unit) -> Indicator :
        
        if not id in self.indicatorsRegistry.key() :
            indicator = self.Indicator(id, frequency, freqDesc, geogLocation, indicatorGroup, unit)
            self.indicatorsRegistry.add[id] = indicator

        return self.indicatorRegistry(id)
    
    def createMeasurementType(self, id : int, description : str) -> MeasurementType :
        
        if not id in self.measurementTypeRegistry.key() :
            measurementType = MeasurementType(id, description)
            self.measurementTypeRegistry.add[id] = measurementType

        return self.measurementTypeRegistry(id)
    
    #devrait vérifier qu'un objet existant n'est pas le même que celui que l'on veut créer
    def createMeasurement(self, id: int, year: int, value: float, timeperiodId: int, timeperiodDesc: str, type: MeasurementType, commodity: Commodity, indicator: Indicator) -> Measurement :

        return self.Measurement(id, year, value, timeperiodId, timeperiodDesc, type, commodity, indicator)