# Classe représentant les indicateurs


import IndicatorGroup
import Unit


class Indicator:
    
    
    def __init__(self, id : str, frequency : int, frequencyDesc : str, geogLocation : str, indicatorGroup : IndicatorGroup, unit : Unit):
        self.id = id
        self.frequency = frequency
        self.frequencyDesc = frequencyDesc
        self.geogLocation = geogLocation
        self.IndicatorGroup = indicatorGroup
        self.Unit = unit