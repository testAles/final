# Classe représentant les cultures vivières, classées par type

import CommodityGroup

class Commodity:
    

    def __init__(self, group : CommodityGroup, id : int, name : str):
        
        self.id = id
        self.name = name